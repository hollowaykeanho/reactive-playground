# Reactive Programming Playground
This is an app forked from [raywenderlich.com](https://www.raywenderlich.com/62699/reactivecocoa-tutorial-pt1)
on using ReactiveCocoa pod. The purpose of this exploration is to learn about
using signals instead of the traditional callbacks and code blocks.


<br><br>
## MVVM exploration
This is one result section from the MVVM framework exploration.


<br><br>
## License
The original repository copyright belongs Colin Eberhardt from raywenderlich.com.


<br><br>
## References
1. https://www.raywenderlich.com/62699/reactivecocoa-tutorial-pt1
2. https://yalantis.com/blog/how-i-learned-to-write-custom-signals-in-reactive-cocoa/
3. https://www.binpress.com/tutorial/reactivecocoa/20
4. http://www.teehanlax.com/blog/krush-ios-architecture/